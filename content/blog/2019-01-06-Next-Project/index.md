---
title: Next Project
date: "2020-01-06T13:00:52+0100"
description: "My next project will be a semi-professional platform with the aim
to cost not too much and basing on a lot of new hot shit. For example podman,
envoy, prometheus, terraform, ansible and so on."
---

I want to create a platform for my new projects. I hope to create something
that will be reusable. But I will focus on learning new stuff. I've heard about
some cool new software and projects and some of them seems to be really cool to
create a platform.

I don't have so much experience with docker and my last playing time with it is
now sometime ago. I've heard it made big process and is nearly stable and
usable in production ready environments. I dislike the fundamental rule that
the docker-daemon have to run as root and my processes are running as child of
a process with root privilege. Its a long time ago we wrote software which was
started and maintained with root privilege on machines, but as the web raises
it started to get a so big security breach, that nobody who cares about
security is doing it anymore. I'm application developer so my understanding
from security and feeling for insecure and secure isn't so remarkable but
that's a rule I've learned at the very beginning.

I want to use docker to learn it a little bit and trying to understand the hype
around it. But this project is a hobby so I don't have the resources and
knowledge to harden everything. So first of all I've had searched for a
software which allows me to start docker images as rootless and is actively
maintained. I've found [rkt](https://github.com/rkt/rkt) but it seems to be
dead, another solution was [kubernetes](https://kubernetes.io/)(k8s) but I
don't have a team to support my machines and platform so its a way to complex
and big, last but not least I've found
[podman](https://github.com/containers/libpod) its active, its starting to get
into the main repositories of the most distributions for example
[debian](https://github.com/containers/libpod/issues/1742) and
[other](https://podman.io/getting-started/installation) so I decided to try
podman.

# Podman
I'm a fanboy of debian and so the first step was to get podman up and running
in debian machine. I've found this
[blog](https://computingforgeeks.com/how-to-install-podman-on-debian/) it was
working and I didn't found any blocker to continue my way along the road. So it
was time to get a debian package to install podman on my debian server. Its
mainly maintained by red hat folks so it wasn't surprising for not to find a
debian package on the official installation instruction site. After some
researched I've found this
[issue](https://github.com/containers/libpod/issues/1742) and hoped to find a
package repository in the comments. But there wasn't any I wanted to use. So
I've started to create my own package.

After some hours of work I've had a package working but I wasn't satisfied with
the quality. So I've wasted a lot of time and wasn't any step nearer my goal.
So back to the beginning. After some searching and better reading of the issue
I found [salsa](https://salsa.debian.org/debian/libpod) it seems to be the
official repository for the debian package for podman. First step was to clone
the repository and trying to get the debian package build. After reading the
[.gitlab-ci.yml](https://salsa.debian.org/debian/libpod/blob/master/debian/.gitlab-ci.yml)
and some docker container later I've had a working packaging process.

The quality of the package and the packaging process was satisfying and so I
created a mirror of the repository and started to create my own
[gitlab-ci](https://gitlab.com/leifwind-thirdparty/libpod/blob/leifwind/.gitlab-ci.yml)
workflow.

## Creating a backport repository
My goal was to create a backport repository for debian buster. I
want to install podman on a buster server and don't want to transfer every
needed package. So first of all I have to create the debian package for podman:

    - mk-build-deps -t "apt-get -o Debug::pkgProblemResolver=yes -y --no-install-recommends" -r -i debian/control
    - origtargz --clean && origtargz --tar-only
    - rm -rf .git
    - ${DEBUILD_CMD}

With the command
[mk-build-deps](https://manpages.debian.org/jessie/devscripts/mk-build-deps.1.en.html)
I can create a debian-package with all build-dependencies. I have to override
the tool because the command is running in a ci-pipeline and we don't have a
prompt to confirm any installation. The tool is like the default tool-command
except the option `-y`, which ensures I'll not asked for any question. With
`-r` I tell `mk-build-deps` to delete the package after installing it and with
`-i` I tell him to install it. The last argument is the path to the debian
control file. After this command is finished successfully all
build-dependencies are installed on the machine.

After installing all build-dependencies I have to fetch the original
source-code and `origtargz --clean && origtargz --tar-only` uses the rules in
[watch](https://gitlab.com/leifwind-thirdparty/libpod/blob/leifwind/debian/watch)
to fetch
them.([manpage](https://manpages.debian.org/buster/devscripts/origtargz.1.en.html)
of `origtargz`)

So with this little trick I have the source-code and all build-dependencies.
Last but not least I've to build the debian package it self.
`eval DEB_BUILD_OPTIONS=nocheck debuild --no-sign` is the command I use for it.
I saved the command in a environment variable that's why I need the `eval` the
variable `DEB_BUILD_OPTIONS=nocheck` is needed to tell `debuild` not to run the
unit tests. Podman is very near on system-level so it uses some features in the
unit tests we don't have in a docker container. My gitlab-ci pipeline is
running in a docker container, so I'm not able to run the tests. The option
`--no-sign` tell `debuild` not to sign the source-code or the changes with a
`gpg-key`.

Before I'm building the package I remove the .git folder because the .git
folder shouldn't be in a debian package. If I'm not deleting it, the folder
will be stored in the debian package.

So when every thing works, I have a debian package for podman. Now we want to
prepare the backport repository.

    - mkdir "public"
    - ls -l
    - cp ../*.deb "public"
    - cd "public"
    - apt -y install ./*.deb
    - apt-cache depends --recurse --no-recommends --no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances podman podman-dbgsym slirp4netns fuse-overlayfs | grep -v podman | grep "^\w" | sort -u > podman_depends
    - apt-get download $(cat podman_depends | sort -u)
    - rm podman_depends
    - dpkg-scanpackages . | gzip -9c > Packages.gz

First of all I'm creating the folder for the artifacts. Afterwards I print the
folder content to get some debug information in my ci-pipeline, copy the
built debian packages into the artifacts folder, change directory to the
artifacts folder, install `podman` and `podman-dbgsym`.

These steps will install the built packages we need them installed to proceed
with the next step. I've found this little command on
[stackoverflow](https://stackoverflow.com/questions/22008193/how-to-list-download-the-recursive-dependencies-of-a-debian-package?answertab=active#tab-top)
and its working like expected.

Now we have the `podman`, `podman-dbgsym` and all dependencies as debian
packages in the public folder. We will upload them as artifacts to the next
process.

    deploy:unstable:
      image: ruby
      stage: deploy
      needs: ["build:unstable"]
      dependencies:
        - "build:unstable"
      script:
        - gem install package_cloud
        - package_cloud push chickeaterbanana/libpod/debian/buster public/*.deb --skip-errors

At the build-step I used a debian image to build, install and prepare the
debian package. Now I'm using a ruby image to install and use the package_cloud
command. The package_cloud-cli is used to push all debian packages into a
debian repository at [packagecloud.io](https://packagecloud.io). Its the
first time I've used [packagecloud.io](https://packagecloud.io) and it was
easy and have done everything i wanted. Next time I need a fast and stable way
to get a semi-private repository(only free for public repositories) I'll using
it again. The option `--skip-errors` is needed to ignore already uploaded
debian packages.

### TODO: Update debian package on a new pipeline run
The ci-pipeline isn't failing but my debian packages won't be updated because
`--skip-errors` ignore already uploaded packages. Maybe the better way would be
to delete the package first and then upload it again.

### TODO: minimal needed packages
The backport repository has every package needed to install podman. But we
already know that the repository is for debian buster. We don't need all
packages to be in the repository, the minimal set of packages would be okay.
But after some research I didn't find a fast way to find out which packages are
needed. On my way down into the rabbit hole dependencies of debian packages
I've found
[debtree](https://manpages.debian.org/buster/debtree/debtree.1.en.html) and
I've created some dependency graphs:

![build-dependencies](./libpod_build.svg)

![build-dependencies max depth 3](./libpod_build_depth_3.svg)

![binary-dependencies](./podman.svg)

# Summary
I've decided to use [podman](https://podman.io) as runtime environment for my
docker container.

I created a [repository](https://packagecloud.io/chickeaterbanana/libpod) for
podman and its dependencies. Updated and created through
[gitlab-ci](https://gitlab.com/leifwind-thirdparty/libpod).

# Next Steps
- decide which image-registry to use
- create a apt-mirror for the server(own+upstream packages)
- create a base-network(DMZ, application-zone)
- create a DNS-Service
...
