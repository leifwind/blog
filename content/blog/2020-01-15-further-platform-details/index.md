---
title: Platform further ideas
date: "2020-01-15T21:01:00+0100"
description: "Some further ideas about my new project. About monitoring,
logging, basic services and the base of my platform."
---

# The Platform

My platform has some basic aims, first of all its a hobby project I don't want
to invest my whole hard earned money only for a platform.  This will influence
my decisions in a hard way. For example Elasticsearch+Kibana is a cool
combination for storing logs. But Elasticsearch+Kibana needs a whole bunch of
services if you want to install in a production-grade(3 master-, 2 data- and 2
kibana-nodes) sometimes you want to use api-nodes in front of your cluster.
The needed resources are really okay if you have some server and want to store
your data in the cluster but for me it means ~5 virtual machines(2 data, 3
(master+kibana)) for storing log-data. I don't think that this will helpful for
my aim not to spend too much money. On the other side in times of
cloud-provider and the speed to setup up and destroy virtual machines maybe
I'll prepare everything in a way to startup a elasticsearch/kibana-stack when I
need one and the other time its destroyed and only the recipes are available.
We'll see.

## What do I expect from a platform?

A platform should be helpful and transparent/invisible. It should give you the
possibilities you need without forcing you to use things you don't need. It
unifies common problems and helps you to centralize the view of your
applications. It scales up and *down* with your needs without big handwork.
For example should every platform give you easy possibilities to create
backups and store them multiple times. Another common feature is
metric-collection and visualization or log collection and storing.

A platform should give you multiple ways to monitor your application. Metrics,
logs, check-commands and so on. Some of you maybe knows the different types of
monitoring also as whitebox and blackbox monitoring.

### whitebox monitoring

The whitebox monitoring based on the knowledge and inside view of your
application. You know everything about your application and knows how long a
call runs and how much data it saves or handles. You know what ways the data is
flowing in platform and where the bottleneck are. You collect data about every
step and checks if the step is performing like expected.

For example if you have a log-unification pipeline, the start will be the
server with systemd-logs, some service collects them and sends this data to a
aggregator and at the end of the pipeline you store the logs in a
database/storage.

A whitebox monitoring know how much log-entries are consumed from systemd, what
bandwidth is needed to send the data from the server to the aggregator and
how long the aggregation task takes. It knows how much data can be processed
in maximum and can notify you if the volume of log-entries is bigger than the
bandwidth of the server, so you can't send all logs to the aggregator. You
know really exact what is going on and you can look into single steps of the
process.

### blackbox monitoring

The blackbox monitoring doesn't know anything about your application. It only
looks at the environment of your application to estimate the utilization of
your application. The same log-unification pipeline in blackbox monitoring will
look at the cpu-time the collector needs, sees the amount of data which is
written to disc on the server and sees how much outgoing and incoming traffic
is handled by the network-card. It can see if your application uses the whole
bandwidth of the network-card or estimate when the disc will be full if no data
is growing in the same speed.

# Conclusion

My aim is to find a suitable solution for blackbox, whitebox monitoring, log
unification, backup and notifications. For this reason i have researched a bit
and find a lot of interesting projects. It seems that
[pagerduty](https://www.pagerduty.com/) is one of the standard ways to get
notifications done. For around 10$ a month you get unlimited SMS notifications
for 6 team member. As log unification layer is [Fluentd](https://fluentd.org)
or [Fluent Bit](https://fluentbit.io/).

If I want to store my backups, a object store is very common and handy. After
some research [S3](https://aws.amazon.com/de/s3/) is one of the most used
object store. After some calculation I estimate to need ~1-3 TB of backup space
for my data. For [S3](https://aws.amazon.com/de/s3/) I need to
[pay](https://calculator.s3.amazonaws.com/index.html#r=FRA&s=S3&key=files/calc-0d73f8a409fbe6d208ada8862bd223585b663546&v=ver20200107wD)
~23-75$ per month for standard object store. An alternative is
[backblaze](https://www.backblaze.com/) after some calculation ~6-16$ per
month. One of the big problems with [backblaze](https://www.backblaze.com) is
the compatibility and integration's to other software components. So if I want
to combine the better price of [backblaze](https://www.backblaze.com) and
[S3](https://aws.amazon.com/de/s3/) as protocol I have found
[minio](https://min.io/). [minio](https://min.io/) can act as gateway for
[b2](https://github.com/minio/minio/blob/master/docs/gateway/b2.md).

On my research to upload backup files to a object store I found
[rclone](https://rclone.org/overview/). It seems very useful to copy/move files
from local/object store to a remote object store. Very useful to speedup the
upload process I can use `--checksum` to compare the hash of the files.
