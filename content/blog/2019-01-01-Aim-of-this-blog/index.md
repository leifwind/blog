---
title: Aim of this blog
date: "2020-01-01T16:21:21+0100"
description: "Aim of this blog"
---

My written English is terrible and i want to train it a bit. I think the best
way to train something is to do it. So if i want to learn writing better
English, i have to write English. I don't know what Topics will arrive in this
blog but i am sure it will have to do with software development. I am
professional software engineer at solute GmbH in Germany the most popular brand
we are working at is [billiger.de](https://billiger.de). My git repositories
live in [gitlab.com](https://gitlab.com/bbruhn).

So this blog is build with [gatsbyjs](https://gatsbyjs.org). I'm backend
developer so my knowledge about frontends isn't so big. I'm sure i do not want
a wordpress or similar to write my blog. So i have searched for a static
website generator. I'm not planing to introduce some dynamic content so static
websites will be totally okay for me. But pure HTML writing is time intensive
and needs a lot of knowledge about CSS and styling of websites. But i only want
to write a blog and not learn the whole stack needed to develop a modern
Web-Blog. So i have searched for a tool to generate static HTML and CSS based
on markdown. A colleague of mine had suggested
[gatsbyjs](https://gatsbyjs.org). I think [gatsbyjs](https://gatsbyjs.org) will
fulfill my requirement and a lot of more.
